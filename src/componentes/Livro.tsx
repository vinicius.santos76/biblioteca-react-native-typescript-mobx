import * as React from 'react';
import { Image } from 'react-native';
import { Button } from 'react-native';
import { inject, observer } from 'mobx-react';
import { IStore } from './store/Store';
import {Card} from 'react-native-elements'

export interface ILivros{
    ID: number
    TituloLivro: string
    Autor: string
    Conteudo: string
    Imagem: ImageData
    Valor: number
    Genero: string
}

@inject("StoreAPP")
@observer
export default class Livro extends React.Component<ILivros & IStore> {

  public render() {
    console.log("Console.log")
    console.log(this.props.StoreAPP.ArrayLivrosCarrinho)
    console.log("*------------*")
    return (
      <Card
        containerStyle={Style.LivroCard}
      >
        <Image 
          source={this.props.Imagem}
          style={Style.ImgLivro}
        />
        <Button
         title={"R$ " + this.props.Valor.toFixed(2)}
         onPress={this.props.StoreAPP.AddItemCarrinho(this.props)}
        />
      </Card>
    );
  } 
}

const Style = {
  ImgLivro: {
    width: 150,
  },
  LivroCard: {
    margin:5,
    padding: 2
  }
}

