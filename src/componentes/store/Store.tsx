import { observable, action, computed } from 'mobx';
import DadosBiblioteca from '../../assets/json/DadosBiblioteca';
import Generos from '../../assets/json/Generos.json'
import { ILivros } from '../Livro'

export interface IStore {
  StoreAPP: Store
}

class Store {
  @observable public ValorTotalCompras: number = 0; // Propriedade visivel a outras classes através do Mobx
  @observable public ArrayLivrosCarrinho: Array<ILivros> = []
  @observable public DadosBiblioteca: Array<ILivros> = DadosBiblioteca
  @observable public Generos: Array<string> = Generos
  @observable public GenerosSelecionados: Array<string> = []
  @observable public SnackBarkOpened: boolean = false
  @observable public SnackBarMessage: string = ""
  @observable public DialogComprasOpened: boolean = false
  @observable public DialogDetalhesLivro: boolean = false

  @action.bound
  public AddItemCarrinho = (Livro: ILivros) => () => {
    // Adiciona o Objeto atual (livro) ao Array de itens no carrinho
    this.ArrayLivrosCarrinho = this.ArrayLivrosCarrinho.concat(Livro)
    this.addValor(Number(Livro.Valor))
    this.SnackBarMessage = "Livro " + Livro.TituloLivro + " adicionado ao carrinho"
    this.handleClickOpenSnack()
  }

  @action.bound
  public RemoveItemCarrinho = (Livro: ILivros) => () => {

    // Coleta Indice do primeiro item do carrinho que tem o Titulo igual ao titulo do item a ser removido
    const IndiceLivro = this.ArrayLivrosCarrinho.map((ArrayFrag: ILivros) => {
      return ArrayFrag.TituloLivro
    }
    ).indexOf(Livro.TituloLivro)

    // Remove o item do array baseado no indice
    this.ArrayLivrosCarrinho.splice(IndiceLivro, 1)

    // Retira o valor dele do contador geral de valor
    this.retiraValor(Number(Livro.Valor))

    // Adiciona mensagem na SnackBar
    this.SnackBarMessage = "Livro " + Livro.TituloLivro + " foi removido do carrinho"

    // Chama a funao da SnackBar que faz com que ela apareça
    this.handleClickOpenSnack()

    // Caso o número de livros for = 0 fecha caixa de dialogo do resumo compras
    if(this.ArrayLivrosCarrinho.length === 0)
    {
      this.DialogComprasOpened = false
    }
  }

  // Funções usadas para manipular a SnackBar
  @action.bound
  public handleClickCloseSnack = () => () => {
    this.SnackBarkOpened = false
  }
  // --

  public handleClickOpenSnack() {
    this.SnackBarkOpened = true
  };

  @action.bound
  public addValor(valor: number) {
    this.ValorTotalCompras = valor + this.ValorTotalCompras
  }

  @action.bound
  public retiraValor(valor: number) {
    this.ValorTotalCompras = this.ValorTotalCompras - valor
  }

  @action.bound
  public FilterLivrosGenero() {
    // Cria constante com dados da biblioteca completos pois os dados de this.DadosBiblioteca são para rendereziração
    const DadosBibliotecaCompletos: Array<ILivros> = DadosBiblioteca.livros

    // Caso não houver nenhum checkbox selecionado apenas atribui todos os dados para this.DadosBiblioteca
    if (this.GenerosSelecionados.length === 0) {
      this.DadosBiblioteca = DadosBibliotecaCompletos

    } else {
      /* Caso algum checkbox diferente de "todos" for selecionado filtra o DadosBiblioteca
         para retornar um array de livros com dentro dos generos selecionados*/
      const ItensFiltrados = DadosBibliotecaCompletos.filter((livro: ILivros) => {

        // Caso o indice do genero do livro dentro do array seja maior que -1 (ou seja exista) irá adicionar o item ao novo array
        return this.GenerosSelecionados.indexOf(livro.Genero) > -1

      })

      // Seta observable DadosBiblioteca da Store com os Livros filtrados por gênero
      this.DadosBiblioteca = ItensFiltrados
    }
  }

  @computed get QtdeTotalLivros(): number {
    return this.ArrayLivrosCarrinho.length
  }


  // Retorna um Array de livros sem livros repetidos
  @computed get LivrosUnicos(): Array<ILivros> {

    // Realiza filtro para retornar somente livros que passem no teste
    const ArrayLivrosUnicos = this.ArrayLivrosCarrinho.filter((livro: ILivros, index: number) => {

      // Retorna true ou false para verificar se o livro é o ptimeiro com este titulo no array
      return this.ArrayLivrosCarrinho.map((ArrayFrag: ILivros) => {
        return ArrayFrag.TituloLivro
      }
      ).indexOf(livro.TituloLivro) === index

    })
    return ArrayLivrosUnicos
  }

  /*Função acionada sempre que uma reação for acionada*/
  @computed get getDesconto() {

    /*Verifica a quantidade de itens unicos e retorna o valor do desconto*/
    switch (this.LivrosUnicos.length) {
      case 0:
        {
          return 0
        }
      case 1:
        {
          return 0
        }
      case 2:
        {
          return 0.05
        }
      case 3:
        {
          return 0.1
        }
      case 4:
        {
          return 0.2
        }
      default:
        {
          return 0.25
        }
    }

  }
}

// const StoreApp = new Store()
export default Store