import * as React from 'react'
import Livro, { ILivros } from './Livro'
import { inject, observer } from 'mobx-react';
import { IStore } from '../componentes/store/Store'
import { View, ScrollView } from 'react-native';
import DetalhesLivro from './DetalhesLivro';

@inject("StoreAPP")
@observer
class ContainerLivros extends React.Component<IStore> {
    public render() {
        const livros = this.props.StoreAPP.DadosBiblioteca.map((objLivro: ILivros, index: number) => {

            return (
                <View key={index} >
                    <Livro
                        {...objLivro}
                    />
                    <DetalhesLivro
                        {...objLivro}
                    />
                </View>
            )
        })

        return (


            <ScrollView
                contentContainerStyle={{ 
                    display: 'flex',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    padding:10,
                    marginTop:10,
                }}
            >
                {livros}
            </ScrollView>
        )
    }
}

export default ContainerLivros as React.ComponentType<{}>

