import { ILivros } from "../../componentes/Livro";

   const DadosBiblioteca: Array<ILivros> = [
        {
            "ID": 1,
            "TituloLivro": "Magisterium a Chave de de Bronze",
            "Autor": "Cassandra Clare & Holly Black",
            "Conteudo": "Aqui, Holly Black e Cassandra Clare nos presenteiam com uma escola em que qualquer coisa boa ou ruim pode acontecer, e o único jeito de desvendar a verdade é arriscando tudo para encontrá-la. Call, Tamara e Aaron deveriam estar preocupados com coisas normais na vida de jovens aprendizes de mago. Ao invés disso, depois da assustadora morte de um de seus colegas de classe, eles devem rastrear um terrível assassino... e arriscar suas próprias vidas no processo. ",
            "Imagem": require("../images/AChavedeBronze.jpg"),
            "Valor": 37.90,
            "Genero": "Literatura Juvenil"
        },
        {
            "ID": 2,
            "TituloLivro": "Maze Runner - Arquivos",
            "Autor": "James Dashner",
            "Conteudo": "Neste volume o leitor terá acesso a documentos confidenciais: e-mails entre os funcionários do CRUEL, memorandos que deveriam ter sido destruídos logo após serem lidos e uma seleção das lembranças de alguns Clareanos. Todos esses arquivos compõem mais esta joia escrita por James Dashner e ilustrada por Marcelo Orsi Blanco, que oferece um olhar único para o mundo de Maze Runner e uma leitura obrigatória para os fãs da saga. ",
            "Imagem": require("../images/ArquivosSecretos.png"),
            "Valor": 19.99,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 3,
            "TituloLivro": "Harry Potter e A Pedra Filosofal",
            "Autor": "Rowling, J. K.",
            "Conteudo": "Harry Potter é um garoto cujos pais, feiticeiros, foram assassinados por um poderosíssimo bruxo quando ele ainda era um bebê. Ele foi levado, então, para a casa dos tios que nada tinham a ver com o sobrenatural. Pelo contrário. Até os 10 anos, Harry foi uma espécie de gata borralheira: maltratado pelos tios, herdava roupas velhas do primo gorducho, tinha óculos remendados e era tratado como um estorvo. ",
            "Imagem": require("../images/APedraFilosofal.jpg"),
            "Valor": 30.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 4,
            "TituloLivro": "A Casa de Hades - Os Heróis do Olimpo",
            "Autor": "Rick Riordan",
            "Conteudo": "Hazel está diante de uma encruzilhada. As forças de Gaia estão decididas a impedi-los de avançar e alcançar seu objetivo: chegar à Casa de Hades, nas terras antigas, para resgatar Percy e Annabeth e fechar definitivamente as Portas da Morte, impedindo os monstros de retornarem ao mundo mortal.",
            "Imagem": require("../images/ACasaHades.jpg"),
            "Valor": 26.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 5,
            "TituloLivro": "O Mago De Batalha",
            "Autor": "Matharu Taran",
            "Conteudo": "A habilidade de um jovem em conjurar demônios vai mudar o destino de um império. O último livro da trilogia ConjuradorDepois de escapar da pirâmide sagrada, no coração do mundo órquico, Fletcher e seus amigos estão presos no éter. Agora precisam voltar para casa, enquanto enfrentam os inimigos mais aterradores. ",
            "Imagem": require("../images/MagoBatalha.jpg"),
            "Valor": 28.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 6,
            "TituloLivro": "Caixa De Pássaros",
            "Autor": "Josh Malerman",
            "Conteudo": "Romance de estreia de Josh Malerman, “Caixa de Pássaros” é um thriller psicológico tenso e aterrorizante, que explora a essência do medo. Uma história que vai deixar o leitor completamente sem fôlego mesmo depois de terminar de ler.Basta uma olhadela para desencadear um impulso violento e incontrolável que acabará em suicídio. ",
            "Imagem": require("../images/CaixaPassaros.jpg"),
            "Valor": 27.90,
            "Genero":  "Horror"
            
        },
        {
            "ID": 7,
            "TituloLivro": "Terra Morta - Relatos de Sobrevivência A Um Apocalipse Zumbi",
            "Autor": "Tiago Toy",
            "Conteudo": "O apocalipse zumbi chegou e ninguém está seguro. Por mais que você se prepare, sinto informar, nada será suficiente. Estocar comida, instalar um teto solar ou aliar-se às pessoas certas pode soar como sobrevivência garantida, mas sugiro que se aproxime e ouça o que tenho a dizer: este é o fim do mundo como o conhecemos.",
            "Imagem": require("../images/TerraMorta.jpg"),
            "Valor": 34.90,
            "Genero":  "Horror"
            
        },
        {
            "ID": 8,
            "TituloLivro": "Cartas da Prisão De Nelson Mandela",
            "Autor": "Shan Venter",
            "Conteudo": "Cartas da prisão de Nelson Mandela é uma obra histórica: a primeira – e única – coleção autorizada de correspondências que abarca os vinte e sete anos em que o líder sul-africano esteve encarcerado. Lançada simultaneamente em diversos países, a publicação celebra o centenário de Mandela. Comoventes, fervorosas, arrebatadoras e sempre inspiradoras, as mais de duzentas cartas – muitas das quais nunca vistas pelo público – foram reunidas a partir de coleções públicas e privadas.",
            "Imagem": require("../images/MandelaHeroi.jpg"),
            "Valor": 67.90,
            "Genero": "Biografia"
            
        },
        {
            "ID": 9,
            "TituloLivro": "Escândalo Na Boêmia E Outros Contos Clássicos De Sherlock Holmes",
            "Autor": "Arthur Conan Doyle",
            "Conteudo": "Esta antologia reúne 15 contos escolhidos para sintetizar o cânone das aventuras escritas por Conan Doyle para a Strand Magazine, revista britânica conhecida por levar a um público amplo grandes nomes da literatura, como Agatha Christie, Rudyard Kipling e Graham Greene. Dos mistérios em quartos fechados a planos mirabolantes de assalto; de intrigas internacionais a casos de espionagem capazes de derrubar o governo (e talvez a Realeza); sem deixar de lado chantagens, perseguições e assassinatos a sangue-frio. ",
            "Imagem": require("../images/EscandaloSherlock.jpg"),
            "Valor": 39.99,
            "Genero":  "Suspense"
            
        },
        {
            "ID": 10,
            "TituloLivro": "Frankenstein - o Médico e o Monstro - Drácula",
            "Autor": "Mary Shelley",
            "Conteudo": "Nesta edição, reunimos três clássicos do terror mundial. 'Frankenstein' (1818), da autora inglesa Mary Shelley, tornou-se um dos maiores clássicos da literatura de terror e conta a história do dr. Victor Frankenstein. Essa história ficou imortalizada no teatro e no cinema em várias adaptações. 'O médico e o monstro' (1886) aborda a dialética dos valores morais em forma assombrosa e vai além do bem e do mal da alma humana. O escritor escocês Robert Louis Stevenson impregnou neste romance um forte clima de apreensão, suspense, terror e perplexidade, que garantem a atenção do leitor do início ao fim.",
            "Imagem": require("../images/FrankDracula.jpg"),
            "Valor": 56.90,
            "Genero": "Ficção Científica"
            
        },
        {
            "ID": 11,
            "TituloLivro": "Javascript e Jquery - Desenvolvimento de Interfaces Web Interativas ",
            "Autor": "Jon Duckett",
            "Conteudo": "Bem-vindo ao ensino de JavaScript & jQuery. Você é iniciante em JavaScript, ou adicionou scripts a sua página web mas quer entender melhor como tudo funciona? Então este livro é para você. Não mostraremos apenas como ler e escrever em JavaScript, mas também ensinaremos o básico sobre programação de computadores de forma simples e visual. Tudo o que você precisa é entender um pouco sobre HTML e CSS. Este livro ensinará como tornar seus websites mais interativos, atraentes, e funcionais. Isso acontece ao combinar teoria de programação com exemplos que demonstram como o JavaScript e o jQuery são usados em sites populares. ",
            "Imagem": require("../images/JavascriptJquery.png"),
            "Valor": 205.90,
            "Genero": "Informática"
            
        },
        {
            "ID": 12,
            "TituloLivro": "Deadpool - Antologia ",
            "Autor": "Rob Liefield",
            "Conteudo": "O programa Arma X atribui a Wade Wilson um fator de regeneração que lhe permite curar-se de um câncer, mas o deixa desfigurado para sempre. A partir daí, ele se torna Deadpool, anti-herói mentalmente instável, mercenário e incorrigível tagarela. Hoje, Wade Wilson é um dos personagens mais famosos da Marvel, um fenômeno mundial, assim como da cultura pop à qual ele é tão obcecado. Esta coletânea reúne os momentos mais marcantes de sua tumultuosa existência e permite a observação de sua evolução no decorrer dos anos.",
            "Imagem": require("../images/DeadpoolAntologia.jpg"),
            "Valor": 126.00,
            "Genero": "HQ"            
        },
        {
            "ID": 13,
            "TituloLivro": "Sempre Faço Tudo Errado Quando Estou Feliz",
            "Autor": "Raquel Segal",
            "Conteudo": "'Ansiedade. Paranoias. Medos. Frustrações. Amor. Sonhos. Desilusões. Expectativas. Aquela vontade louca de desistir de tudo. Recomeçar. Desistir de novo. Sentir-se inteiro. Vazio. Transbordar. Se perder. Pode parecer, mas você não está sozinho...Em Sempre faço tudo errado quando estou feliz, Raquel Segal, criadora do Aquele Eita, fala de emoções reais, destas que a gente só conta para o travesseiro. É impossível não se impactar com seu traço revelador e, ao mesmo tempo, transformador.'",
            "Imagem": require("../images/SempreBesteiraFeliz.jpg"),
            "Valor": 17.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 14,
            "TituloLivro": "Uma Dobra No Tempo",
            "Autor": "L'engle Madeleine",
            "Conteudo": "“Noites loucas são a minha glória”, diz a estranha misteriosa. “Foi só uma lufada que me pegou de jeito e me tirou da rota. Descansarei um pouco e seguirei meu rumo. Por falar em rumos, meu doce, saiba que o tesserato existe, sim.”",
            "Imagem": require("../images/DobraNoTempo.jpg"),
            "Valor": 18.90,
            "Genero":  "Literatura Juvenil"
            
        },
        {
            "ID": 15,
            "TituloLivro": "Trono de Vidro - Império de Tempestades - Vol. 5 – Tomo 2 ",
            "Autor": "Maas Sarah J.",
            "Conteudo": "A história de Aelin Galathynius, sempre repleta de ação e intrigas, continua nesta segunda parte do quinto livro da série, Império de tempestades Aelin Galathyius sobreviveu a prisão, à perda de amigos e amores, às traições. Agora deve vencer seu maior medo para salvar o mundo. Com a vida e poder jurados ao povo que está determinada a salvar, a antiga assassina, conhecida como Celaena Sardothien, colocará a própria segurança em risco para proteger os seus. Mais que nunca, Aelin precisa de Rowan, de Dorian e de todos os aliados para conseguir descobrir a localização da relíquia sagrada capaz de banir de seu mundo a ameaça valg e os horrores libertados em Morath. Chegou a hora de levantar os exércitos de Erilea. De cobrar velhas dívidas... É hora de marchar contra o mais supremo dos males. E confiar na pureza de seu coração para trazer a luz.",
            "Imagem": require("../images/TronoDeVidro.jpg"),
            "Valor": 33.90,
            "Genero":  "Literatura Juvenil"
            
        },
        {
            "ID": 16,
            "TituloLivro": "Cidade Das Almas Perdidas - Os Instrumentos Mortais - Vol. 5",
            "Autor": "Clare Cassandra",
            "Conteudo": "Quando Jace e Clary voltam a se encontrar, Clary fica horrorizada ao descobrir que a magia de Lilith, um demônio muito poderoso, ligou Jace ao perverso Sebastian, transformando o Caçador de Sombras em um servo do mal. A Clave decide eliminar Sebastian, mas não há nenhuma maneira de matá-lo sem destruir Jace. Clary e seus amigos, no entanto, irão tentar mesmo assim. Ela está disposta a fazer qualquer coisa para salvar o namorado, mas ainda pode confiar nele? Ou ele está realmente perdido?",
            "Imagem": require("../images/CidadeDasAlmas.png"),
            "Valor": 9.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 17,
            "TituloLivro": "Um Cadáver Ouve Rádio",
            "Autor": "Rey Marcos",
            "Conteudo": "Com uma linguagem moderna, simples e coloquial, o livro Um cadáver ouve rádio, de Marcos Rey, acaba de ser relançado pela Global Editora. Indicado para o público infanto-juvenil, a obra retrata a história de um misterioso assassinato em um prédio em construção abandonado. Tudo começa quando um garoto chamado Muriçoca para na entrada do prédio para se proteger da chuva. Atraído pelo som de um frevo, sobe as escadas e se depara com o corpo de um homem caído de costas, todo ensanguentado.",
            "Imagem": require("../images/CadaverRadio.jpg"),
            "Valor": 9.90,
            "Genero":  "Suspense"
            
        },
        {
            "ID": 18,
            "TituloLivro": "Jogador Nº1",
            "Autor": "Cline Ernest",
            "Conteudo": "O ano é 2044 e a Terra não é mais a mesma. Fome, guerras e desemprego empurraram a humanidade para um estado de apatia nunca antes visto. Wade Watts é mais um dos que escapa da desanimadora realidade passando horas e horas conectado ao OASIS – uma utopia virtual global que permite aos usuários ser o que quiserem; um lugar onde se pode viver e se apaixonar em qualquer um dos mundos inspirados nos filmes, videogames e cultura pop dos anos 1980. ",
            "Imagem": require("../images/Jogador1.jpg"),
            "Valor": 24.90,
            "Genero": "Suspense"
            
        },
        {
            "ID": 19,
            "TituloLivro": "Tower Of Dawn - A Throne Of Glass Novel Uk Edition",
            "Autor": "Maas Sarah J.",
            "Conteudo": "Chaol Westfall has always defined himself by his unwavering loyalty, his strength, and his position as the Captain of the Guard. But all of that has changed since Aelin shattered the glass castle, since Chaol's men were slaughtered, since the King of Adarlan spared him from a killing blow, but left his body broken. Now he and Nesryn sail for Antica - the stronghold of the southern continent's mighty empire and of the legendary healers of the Torre Cesme.",
            "Imagem": require("../images/Tower.jpg"),
            "Valor": 34.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 20,
            "TituloLivro": "Tarde Demais",
            "Autor": "Hoover Colleen",
            "Conteudo": "A autora best-seller do The New York Times está de volta com um romance ainda mais sombrio, intenso e assustadoramente real.Para proteger o irmão, Sloan foi ao inferno e fez dele seu lar. Ela está presa em um relacionamento com Asa Jackson, um perigoso traficante, e quanto mais os dias passam, mais parece impossível enxergar uma saída.",
            "Imagem": require("../images/TardeDemais.jpg"),
            "Valor": 29.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 21,
            "TituloLivro": "The Walking Dead Vol. 21 - Guerra Total - Parte 2",
            "Autor": "Adlard Charlie & Robert Kirkman",
            "Conteudo": "A autora best-seller do The New York Times está de volta com um romance ainda mais sombrio, intenso e assustadoramente real.Para proteger o irmão, Sloan foi ao inferno e fez dele seu lar. Ela está presa em um relacionamento com Asa Jackson, um perigoso traficante, e quanto mais os dias passam, mais parece impossível enxergar uma saída.",
            "Imagem": require("../images/TheWalkingDead.jpg"),
            "Valor": 27.90,
            "Genero": "HQ"
            
        },
        {
            "ID": 22,
            "TituloLivro": "Outsider ",
            "Autor": "Stephen King",
            "Conteudo": "Um crime indescritível. Uma investigação inexplicável. Uma das histórias mais perturbadoras de Stephen King dos últimos tempos.O corpo de um menino de onze anos é encontrado abandonado no parque de Flint City, brutalmente assassinado.",
            "Imagem": require("../images/OutSider.jpg"),
            "Valor": 47.90,
            "Genero": "Suspense"
            
        },
        {
            "ID": 23,
            "TituloLivro": "Godsgrave - O Espetáculo Sangrento ",
            "Autor": "Plataforma 21",
            "Conteudo": "'Nascimento. Vida. E morte. É assim que cantamos a jornada de personagens heroicos. Porém, a dona desta trama, não é uma heroína com a qual se está acostumado. Mia Corvere – o pequeno corvo – é a encarnação da vingança. ",
            "Imagem": require("../images/GodsGrave.jpg"),
            "Valor": 35.90,
            "Genero": "Literatura Juvenil"
            
        },
        {
            "ID": 24,
            "TituloLivro": "Dona Do Poder",
            "Autor": "Plataforma 21",
            "Conteudo": "Ao falhar em obter o trono de Valáquia, Lada Dracul deseja punir a todos que ousarem cruzar seu caminho. Movida pela raiva, ela ataca com seus homens. Mas a força bruta não é suficiente para que Lada conquiste o que deseja. ",
            "Imagem": require("../images/DonaDoPoder.jpg"),
            "Valor": 35.90,
            "Genero": "Literatura Juvenil"
            
        }
    ]

    export default DadosBiblioteca