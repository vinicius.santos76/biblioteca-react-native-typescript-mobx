import 'es6-symbol/implement';
import * as React from 'react';
import { Provider } from 'mobx-react';
import Store from './src/componentes/store/Store';
import ContainerLivros from './src/componentes/ContainerLivros';
import { View } from 'react-native';

export default class App extends React.Component {

  public StoreAPP = new Store()
  render() {
    return (
      <Provider StoreAPP={this.StoreAPP}>
        <View>
          <ContainerLivros />
        </View>
      </Provider>
    );
  }
}
